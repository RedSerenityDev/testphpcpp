#ifndef EXTENSION_FUNCTIONS_H
#define EXTENSION_FUNCTIONS_H

#include "Zend.h"
#include "RSZend.h"

class ExtensionFunctions : public Php::Functions {
	public:
		ExtensionFunctions();
		static void TestFunction(ZendExecuteData *ExecuteData, zval *return_value) {
			zend_string *strg;

			strg = strpprintf(0, "Congratulations! It worked!");
			RETURN_STR(strg);
		}
};

#endif