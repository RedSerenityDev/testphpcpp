#include "PhpEnhanced.h"
#include "ExtensionFunctions.h"

void PhpEnhancedExtension::ModuleInfo(ZendModuleEntry *Module) {
	php_info_print_table_start();
	php_info_print_table_header(2, "PhpEnhanced Support", "ENABLED, dude.");
	php_info_print_table_row(2, "Version", "0.1.0");
	php_info_print_table_end();
}

void PhpEnhancedExtension::Setup() {
	ExtensionName("PhpEnhanced");
	ExtensionVersion("0.1.0");

	RegisterModuleInfo(&PhpEnhancedExtension::ModuleInfo);
	RegisterFunctions<ExtensionFunctions>();

	//RegisterInterface<MyInterface>();
	//RegisterClass<PhpEnhanced::Exception>();

	std::printf("%s\n", "Setup().");
}

//EXTENSION_ENTRY(PhpEnhancedExtension)

extern "C" {
	RSZEND_EXPORT void *get_module() {
		static PhpEnhancedExtension _Extension;
		_Extension.Setup();
		return _Extension;
	}
}
