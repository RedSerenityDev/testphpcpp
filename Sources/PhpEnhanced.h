#ifndef PHP_ENHANCED_H
#define PHP_ENHANCED_H

#include "Zend.h"
#include "RSZend.h"

class PhpEnhancedExtension : public Php::Extension {

	public:
		static void ModuleInfo(ZendModuleEntry *Module);
		void Setup();

};

#endif