#include "Extension.h"

namespace Php {

	/**
	 *  If this extension is compiled for a PHP version with multi
	 *  threading support, we need an additional header file.
	 */
	#ifdef ZTS
		#include "TSRM.h"
	#endif

	/**
	 * Zend expects a globals definition even if we don't have any globals.
	 *
	 */
	ZEND_BEGIN_MODULE_GLOBALS(rszend)
	ZEND_END_MODULE_GLOBALS(rszend)
	#ifdef ZTS
		#define PHPCPP_G(v) TSRMG(rszend_globals_id, rszend_globals *, v)
	#else
		#define PHPCPP_G(v) (rszend_globals.v)
	#endif
	ZEND_DECLARE_MODULE_GLOBALS(rszend)
	static void InitializeGlobals(zend_rszend_globals *globals) {}


	/**
	 *  Constructor
	 */
	Extension::Extension() {
		// keep extension pointer based on the name
		//name2extension[name] = this;

		/*static const zend_function_entry functionTest[] = {
			PHP_FE_END
		};

		zend_function_entry *fTest = new zend_function_entry[1];
		memset(fTest, 0, sizeof(*fTest));*/

		// assign all members (apart from the globals)
		_entry.size                  = sizeof(ZendModuleEntry);    // size of the data
		_entry.zend_api              = ZEND_MODULE_API_NO;           // api number
		_entry.zend_debug            = ZEND_DEBUG;                   // debug mode enabled?
		_entry.zts                   = USING_ZTS;                    // is thread safety enabled?
		_entry.ini_entry             = NULL;                         // the php.ini record, will be filled by Zend engine
		_entry.deps                  = NULL;                         // dependencies on other modules
		_entry.name                  = NULL;                         // extension name
		_entry.functions             = NULL;                         // functions supported by this module (none for now)
		_entry.module_startup_func   = &Extension::ModuleStartup;    // startup function for the whole extension
		_entry.module_shutdown_func  = &Extension::ModuleShutdown;   // shutdown function for the whole extension
		_entry.request_startup_func  = &Extension::RequestStartup;   // startup function per request
		_entry.request_shutdown_func = &Extension::RequestShutdown;  // shutdown function per request
		_entry.info_func             = NULL;                         // information for retrieving info
		_entry.version               = NULL;                         // version string
		_entry.globals_size          = 0;                            // size of the global variables
		_entry.globals_ctor          = NULL;                         // constructor for global variables
		_entry.globals_dtor          = NULL;                         // destructor for global variables
		_entry.post_deactivate_func  = NULL;                         // unknown function
		_entry.module_started        = 0;                            // module is not yet started
		_entry.type                  = 0;                            // temporary or persistent module, will be filled by Zend engine
		_entry.handle                = NULL;                         // dlopen() handle, will be filled by Zend engine
		_entry.module_number         = 0;                            // module number will be filled in by Zend engine
		_entry.build_id              = (char *)ZEND_MODULE_BUILD_ID; // check if extension and zend engine are compatible

		// things that only need to be initialized
		#ifdef ZTS
		_entry.globals_id_ptr = NULL;
		#else
		_entry.globals_ptr = NULL;
		#endif
		std::printf("%s\n", "After Constructor.");

		/*std::printf("Address of ModuleStartup is %p\n", (void *)(&Extension::ModuleStartup));
		std::printf("Address of ModuleShutdown is %p\n", (void *)(&Extension::ModuleShutdown));
		std::printf("Address of RequestStartup is %p\n", (void *)(&Extension::RequestStartup));
		std::printf("Address of RequestShutdown is %p\n", (void *)(&Extension::RequestShutdown));
		std::printf("Address of InfoTest is %p\n", (void *)(&Extension::InfoTest));
		std::printf("Address of functionTest is %p\n", (void *)(&functionTest));
*/

	}

	/**
	 *  Destructor
	 */
	Extension::~Extension() {
		// remove from the array
		//name2extension.erase(_entry.name);

		// deallocate functions
		delete[] _entry.functions;
	}

	void Extension::RegisterModuleInfo(ZendModuleInfoFunction ModuleInfoFunction) {
		_entry.info_func = ModuleInfoFunction;
	}

	/**
	 *  Function that is called when the extension initializes
	 *  @param  type        Module type
	 *  @param  number      Module number
	 *  @param  tsrm_ls
	 *  @return int         0 on success
	 */
	int Extension::ModuleStartup(int type, int ModuleNumber TSRMLS_DC) {
		// initialize and allocate the "global" variables
		ZEND_INIT_MODULE_GLOBALS(rszend, InitializeGlobals, NULL);

		// get the extension
		//auto *extension = find(module_number TSRMLS_CC);

		// initialize the extension
		std::printf("%s\n", "Extension::ModuleStartup");
		return BOOL2SUCCESS(true);
	}

	/**
	 *  Function that is called when the extension is about to be stopped
	 *  @param  type        Module type
	 *  @param  number      Module number
	 *  @param  tsrm_ls
	 *  @return int
	 */
	int Extension::ModuleShutdown(int type, int ModuleNumber TSRMLS_DC) {
		std::printf("%s\n", "Extension::ModuleShutdown");
		return BOOL2SUCCESS(true);
	}

	/**
	 *  Function that is called when a request starts
	 *  @param  type        Module type
	 *  @param  number      Module number
	 *  @param  tsrm_ls
	 *  @return int         0 on success
	 */
	int Extension::RequestStartup(int type, int ModuleNumber TSRMLS_DC) {
		std::printf("%s\n", "Extension::RequestStartup");
		return BOOL2SUCCESS(true);
	}

	/**
	 *  Function that is called when a request is ended
	 *  @param  type        Module type
	 *  @param  number      Module number
	 *  @param  tsrm_ls
	 *  @return int         0 on success
	 */
	int Extension::RequestShutdown(int type, int ModuleNumber TSRMLS_DC) {
		std::printf("%s\n", "Extension::RequestShutdown");
		return BOOL2SUCCESS(true);
	}

	void Extension::ExtensionName(char *Name) {
		_entry.name = Name;
		std::printf("Extension name is %s\n", _entry.name);
	}

	void Extension::ExtensionVersion(char *Version) {
		_entry.version = Version;
		std::printf("Extension version is %s\n", _entry.version);
	}

	/**
	 *  Retrieve the module entry
	 *  @return zend_module_entry
	 */
	ZendModuleEntry *Extension::Module() {
		std::printf("%s\n", "Sending Module _entry.");
		std::printf("Address of _entry is %p\n", (void *)(&_entry));
		return &_entry;
	}

	/**
	 *  Initialize the extension after it was started
	 *  @param  module_number
	 *  @param  tsrm_ls
	 *  @return bool
	 */
	bool Extension::Initialize(int ModuleNumber TSRMLS_DC) {
		std::printf("%s\n", "Extension::Initialize");
		return true;
	}

	/**
	 *  Function that is called when the extension shuts down
	 *  @param  module_number
	 *  @param  tsrmls
	 *  @return bool
	 */
	bool Extension::Shutdown(int ModuleNumber TSRMLS_DC) {
		std::printf("%s\n", "Extension::Shutdown");
		return true;
	}


}

//#include "Extension.hh"