#ifndef RSZEND_EXTENSION_H
#define RSZEND_EXTENSION_H

#include "Zend.h"

namespace Php {

	class Extension {
		protected:
			/**
			*  The information that is passed to the Zend engine
			*  @var zend_module_entry
			*/
			ZendModuleEntry _entry;

			/**
			*  Is the object locked? This prevents crashes for 'apache reload'
			*  because we then do not have to re-initialize the entire php engine
			*  @var bool
			*/
			bool _locked = false;

			/**
			*  The .ini entries
			*
			*  @var std::unique_ptr<zend_ini_entry_def[]>
			*/
			//std::unique_ptr<zend_ini_entry_def[]> _ini = nullptr;

			void ExtensionName(char *Name);
			void ExtensionVersion(char *Version);
			void RegisterModuleInfo(ZendModuleInfoFunction ModuleInfoFunction);

			template <typename FunctionClass>
			void RegisterFunctions() {
				FunctionClass _Functions;
				_entry.functions = _Functions.AllFunctions();
			}

		public:
			/**
			*  Constructor
			*  Destructor
			*/
			Extension();
			virtual ~Extension();

			/**
			 *  Is the object locked (true) or is it still possible to add more functions,
			 *  classes and other elements to it?
			 *  @return bool
			 */
			bool locked() { return _locked; }

			/**
			 *  Retrieve the module entry
			 *
			 *  This is the memory address that should be exported by get_module()
			 *  function.
			 *
			 *  @return _zend_module_entry
			 */
			ZendModuleEntry *Module();

			/**
			 *  Cast to a module entry
			 *  @return _zend_module_entry*
			 */
			operator ZendModuleEntry * () { return Module(); }

		private:
			/**
			 *  Initialize the extension after it was registered
			 *  @param  module_number
			 *  @param  tsrm_ls
			 *  @return bool
			 */
			bool Initialize(int ModuleNumber TSRMLS_DC);

			/**
			 *  Shutdown the extension
			 *  @param  module_number
			 *  @param  tsrm_ls
			 *  @return bool
			 */
			bool Shutdown(int ModuleNumber TSRMLS_DC);

			/**
			 *  Function that is called when the extension initializes
			 *  @param  type        Module type
			 *  @param  number      Module number
			 *  @param  tsrm_ls
			 *  @return int         0 on success
			 */
			static int ModuleStartup(int type, int ModuleNumber TSRMLS_DC);

			/**
			 *  Function that is called when the extension is about to be stopped
			 *  @param  type        Module type
			 *  @param  number      Module number
			 *  @param  tsrm_ls
			 *  @return int
			 */
			static int ModuleShutdown(int type, int ModuleNumber TSRMLS_DC);

			/**
			 *  Function that is called when a request starts
			 *  @param  type        Module type
			 *  @param  number      Module number
			 *  @param  tsrm_ls
			 *  @return int         0 on success
			 */
			static int RequestStartup(int type, int ModuleNumber TSRMLS_DC);

			/**
			 *  Function that is called when a request is ended
			 *  @param  type        Module type
			 *  @param  number      Module number
			 *  @param  tsrm_ls
			 *  @return int         0 on success
			 */
			static int RequestShutdown(int type, int ModuleNumber TSRMLS_DC);

	};

}

#endif