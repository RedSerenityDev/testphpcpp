#include "PhpFunctions.h"

namespace Php {

	void Functions::RegisterFunction(char *FunctionName, ZendFunction Function) {
		std::printf("Adding Function of pointer %p\n", (void *)(&Function));
		ZendFunctionEntry _entry;

		_entry.fname = FunctionName;
		_entry.handler = Function;
		_entry.arg_info = NULL;
		_entry.num_args = 0;//sizeof(ArgList);

		_Functions.push_back(_entry);
		std::printf("Address of _entry %p\n", (void *)(&_entry));
	}

	const ZendFunctionEntry *Functions::AllFunctions() {
		int NumOfFunctions = _Functions.size();

		_entries = new ZendFunctionEntry[NumOfFunctions + 1];

		for (int i = 0; i < NumOfFunctions; i++) {
			_entries[i] = _Functions.at(i);
			std::printf("Function pointer in list is %p and %p\n", (void *)(&_entries[i]),(void *)(&_entries[i].handler));
		}

		ZendFunctionEntry *_last = &_entries[NumOfFunctions];
		memset(_last, 0, sizeof(ZendFunctionEntry));

		return _entries;
	}

}