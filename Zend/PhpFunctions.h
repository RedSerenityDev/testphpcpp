#ifndef RSZEND_PHP_FUNCTIONS_H
#define RSZEND_PHP_FUNCTIONS_H

#include "Zend.h"

namespace Php {

	class Functions {

		private:
			ZendFunctionList _Functions;
			ZendFunctionEntry * _entries = nullptr;
		protected:

			void RegisterFunction(char *FunctionName, ZendFunction Function);

		public:

			const ZendFunctionEntry *AllFunctions();
			operator const ZendFunctionEntry * () { return AllFunctions(); }

	};

}

#endif