#ifndef RSZEND_PROPERTIES_H
#define RSZEND_PROPERTIES_H

/**
 * Taken from: http://www.codeproject.com/Articles/33293/C-implementation-of-the-C-Property-and-Indexer-wit
 */

/**
 * class Something {
 * 	public:
 * 		Property<int> MyProperty;
 * }
 */

template <class T>
struct Property {
	T& operator= (const T& newValue) {
		value = newValue; //update zend class property
		return value;
	}

	operator T() const {
		return value; //get zend class property
	}

	protected:
		T value;
};

#endif