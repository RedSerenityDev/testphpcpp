#ifndef RS_ZEND_H
#define RS_ZEND_H

/**
 *  Include standard C and C++ libraries
 */
#include <stdlib.h>
#include <string>
#include <initializer_list>
#include <vector>
#include <map>
#include <set>
#include <memory>
#include <list>
#include <exception>
#include <type_traits>
#include <functional>

// for debug
#include <iostream>

/**
 *  PHP includes
 */
#pragma GCC system_header
#include "php.h"
#include "ext/standard/info.h"
#include "zend_exceptions.h"
#include "zend_interfaces.h"
#include "zend_ini.h"

/**
 *  We don't work with older versions of PHP
 */
#if PHP_VERSION_ID < 70000
	#error "This library requires PHP version 7.0 or higher."
#endif


/**
 * MACRO DEFINITIONS
 */

#if defined _WIN32 || defined __CYGWIN__
	#ifdef BUILDING_RSZEND
		#ifdef __GNUC__
			#define RSZEND_EXPORT __attribute__ ((dllexport))
		#else
			#define RSZEND_EXPORT __declspec(dllexport)
		#endif
	#else
		#ifdef __GNUC__
			#define DLL_EXPORT __attribute__ ((dllimport))
		#else
			#define DLL_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define RSZEND_EXPORT __attribute__ ((visibility ("default")))
#endif

/**
 * Macro to define module entry point
 */
#define EXTENSION_ENTRY(ExtensionClass) \
	extern "C" { \
		RSZEND_EXPORT void *get_module() { \
			static ExtensionClass _Extension; \
			_Extension.Setup(); \
			return _Extension; \
		} \
	}

/**
 *  Macro to convert results to success status
 */
#define BOOL2SUCCESS(b) ((b) ? SUCCESS : FAILURE)



/**
 * Type Definitions
 */

typedef zend_module_entry ZendModuleEntry;
typedef zend_execute_data ZendExecuteData;
typedef void(*ZendModuleInfoFunction)(ZendModuleEntry *Module);
typedef void(*ZendFunction)(ZendExecuteData *ExecuteData, zval *ReturnValue);
typedef zend_function_entry ZendFunctionEntry;
typedef zend_internal_arg_info ZendFunctionParameter;
//typedef std::list<std::shared_ptr<ZendFunctionEntry>> ZendFunctionList;
typedef std::vector<ZendFunctionEntry> ZendFunctionList;




#endif